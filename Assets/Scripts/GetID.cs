﻿using UnityEngine;

public class GetID : MonoBehaviour
{
    public static string ID = "";
    private void Awake()
    {

#if UNITY_EDITOR
        Debug.Log($"advertisingId DDDDFSADSAD-0565655605-78898DS True");
#endif
        Application.RequestAdvertisingIdentifierAsync(
            (string advertisingId, bool trackingEnabled, string error) =>
            {
                ID = advertisingId;
                Debug.Log("advertisingId " + advertisingId + " " + trackingEnabled + " " + error); 
            }
        );
    }
}