﻿using System.Collections.Generic;
using UnityEngine;

public class ClientGenerator
{
    private Client[] _clients = null;
    private List<Client> _clientsPool = new List<Client>();
    private int _maxCount = 15;

    public void SetupClientGenerator(ClientScriptableObjeect[] clientObjs, int maxCount = 15)
    {
        _clients = GetClientsFromObjects(clientObjs);
        _maxCount = maxCount;
        GenerateClientsPool();
    }

    private Client[] GetClientsFromObjects(ClientScriptableObjeect[] clientObjs)
    {
        Client[] clients = new Client[clientObjs.Length];
        for(int i = 0; i < clients.Length; i++)
        {
            clients[i] = clientObjs[i].client;
            clients[i].Initialize(clientObjs[i]);
        }
        return clients;
    }

    private void GenerateClientsPool()
    {
        if (_clientsPool.Count < _maxCount)
            for (int i = 0; i < _maxCount; i++)
                AddClientToPool();
    }

    private void AddClientToPool(int id = -1 , int pos = -1)
    {
        if (_clientsPool.Count >= _maxCount) return;

        if (_clientsPool.Count < _clients.Length)
            id = _clientsPool.Count; // CREATE ALL CLIENTS IN LIST FIRST TIME
        else 
            id = id >= 0 ? id : UnityEngine.Random.Range(0, _clients.Length); // NOW CREATE ANY RANDOM CLIENTS

        pos = pos >= 0 ? pos : UnityEngine.Random.Range( 0, TableController.GetTablesCount());

        if ( _clients == null && _clients[id] == null ) return;

        Client client = _clients[id].Clone();
        _clientsPool.Add(client);
    }

    private Client GetClientFromPool()
    {
        if (_clientsPool == null || _clientsPool.Count < 1) return null;

        for (int i = 0; i < _clientsPool.Count; i++)
            if (!_clientsPool[i].gameObject.activeSelf) return _clientsPool[i];
        
        return null;
    }

    public Client GetClientAtRandomPosition()
    {
        Client client = GetClientFromPool();

        int pos = Random.Range(0, TableController.GetTablesCount());
        client?.SetupPositions(pos, TableController.GetClientPositions(pos));

        return client;
    }

    public Client GetClientAtPosition(int pos, float offsetY = 0)
    {
        if (pos < 0 || pos >= TableController.GetTablesCount()) return null;

        Client client = GetClientFromPool();
        client?.SetupPositions(pos, TableController.GetClientPositions(pos), offsetY);

        return client;
    }

    public int GetActiveClients()
    {
        int active = 0;

        for(int i = 0; i < _clientsPool.Count; i++)
        {
            if (_clientsPool[i].gameObject.activeSelf) active++;
        }

        return active;
    }

    public void ClearClientsPool()
    {
        foreach(var client in _clientsPool)
        {
            MonoBehaviour.Destroy(client.gameObject);
        }

        _clientsPool.Clear();
    }

    public void ResetClients()
    {
        for (int i = 0; i < _clientsPool.Count; i++)
            _clientsPool[i].gameObject.SetActive(false);
    }

    public void UpdateClientRenders()
    {
        float render = 0;
        for (int i = 0; i < _clientsPool.Count; i++)
            if (_clientsPool[i].gameObject.activeSelf)
            {
                render = _clientsPool[i].transform.position.y * -100;
                _clientsPool[i].UpdateOrder((int)render);
            }
    }

    public void FreezeClients()
    {
        for (int i = 0; i < _clientsPool.Count; i++)
            if (_clientsPool[i].gameObject.activeSelf)
                _clientsPool[i].Freeze();
    }
}