﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Rendering;

[RequireComponent(typeof(Animator), typeof(Collider2D), typeof(Sound))]
public class Client : MonoBehaviour
{
    private ClientScriptableObjeect _settings = null;

    private enum ClientState { Simple, Returned, Fighting, Freezed}
    private ClientState _currentState = ClientState.Simple;

    private Transform _startPosition, _endPosition, _hidePosition;
    private Vector3 _currentPosition;
    private Direction _currentDirection = Direction.Down;
    private float _lastTime = 0f;
    private int _currnetTable = 0;

    private int _returnProbability = 0;
    private int _tipsProbability = 0;
    private int _canDrinkBeerMugs = 0;


    private Animator _animator = null;
    private SortingGroup _sortingGroup = null;

    private float _gameSpeed = 1f; // SLOW DOWN COEFFICIENT

    public bool hasBeer { get; private set; } = false;

    // EVENTS
    public static event Action<int, float> OnThrowMug = delegate { };
    public static event Action<int, float> OnTipsPlaced = delegate { };
    public static event Action OnClientLeft = delegate { };
    public static event Action<int> OnClientKickBarmen = delegate { };

    public void Initialize(ClientScriptableObjeect clientSettings)
    {
        _settings = clientSettings;
        GetComponents();
    }

    private void GetComponents()
    {
        _animator = GetComponent<Animator>();
        
        if (!gameObject.TryGetComponent<SortingGroup>(out _sortingGroup))
            _sortingGroup = gameObject.AddComponent<SortingGroup>();

        _sortingGroup.sortingLayerName = "Guest";
    }

    public Client Clone()
    {
        GameObject clientGO = Instantiate(gameObject);
        clientGO.gameObject.SetActive(false);
        Client clone = clientGO.GetComponent<Client>();
        clone._settings = _settings;
        clone._returnProbability = _settings.returnProbability;
        clone._tipsProbability = _settings.tipPlaceProbability;
        clone._canDrinkBeerMugs = _settings.maxBeerMugs;
        clone.GetComponents();
        clone._currentDirection = Direction.Down;
        return clone;
    }

    public void SetupPositions(int tableID, Table.TableClientPoints clientPositions, float offsetY = 0)
    {
        Table.TableClientPoints positions = clientPositions;
        _currnetTable = tableID;
        _startPosition = positions.clientStartPosition;
        _hidePosition = positions.clientHidePosition;
        _endPosition = positions.clientEndPosition;
        _currentPosition = _startPosition.position;
        _currentPosition.y += offsetY;
        transform.position = _currentPosition;
    }

    private void OnEnable()
    {
        if (_settings == null) return;

        _canDrinkBeerMugs = _settings.maxBeerMugs;
        hasBeer = false;
        _currentDirection = Direction.Down;
        AnimatorHasBeer(false);
        AnimatorIsWalking(false);
        _animator.Play(_settings.startAnimation, 0, UnityEngine.Random.Range(0f,1f));
        StartCoroutine(Movning());

        //SLOW MO
        _gameSpeed = FreezeButton.currentSpeed;
        FreezeButton.OnChangeSpeed += SetSpeed;
    }


    private bool CheckLastTime(float delay) => Time.time > _lastTime + delay * 1/_gameSpeed;
    
    private bool IsAtDownPosition() => transform.position.y <= _endPosition.position.y;
    
    private bool IsAtTopPosition()=> transform.position.y > _startPosition.position.y;

    private bool IsAtHidePosition() => transform.position.y > _hidePosition.position.y;

    private IEnumerator Movning()
    {
        var stopForAFewSeconds = new WaitForSeconds(_settings.stopTime);

        while (true)
        {
            if( CheckLastTime(_settings.moveTime) && _currentDirection != Direction.Up) { // FREEZ FOR A FEW TIME
                AnimatorIsWalking(false);
                yield return stopForAFewSeconds;
                _lastTime = Time.time;
            }

            if (IsAtDownPosition () && _currentDirection == Direction.Down) // BOTTOM POINT
            {
                AnimatorIsWalking(false);
                transform.position = _endPosition.position;
                Invoke(nameof(KickBarmen), _settings.kickBarmenDelay);
                yield break;
            }
            else if (IsAtTopPosition() && _currentDirection == Direction.Up) // TOP POINT
            {
                StartCoroutine(MoveUpAndHide());
                yield break; // EXIT
            }
            else  // MOVE FORWARD
            {
                AnimatorIsWalking(true);
                MovePlayer();
            }
            yield return null;
        }
    }

    private void MovePlayer(float? curSpeed = null)
    {
        if(curSpeed == null)
        curSpeed = _currentDirection == Direction.Down ? -1 * _settings.forwardSpeed : _settings.backSpeed;

        _currentPosition.y += (float)curSpeed * Time.deltaTime * _gameSpeed;
        transform.position = _currentPosition;
    }

    private IEnumerator MoveUpAndHide()
    {
        AnimatorIsWalking(false);
        while (!IsAtHidePosition()) // MOVE UP
        {
            MovePlayer();
            yield return null;
        }

        AnimatorFlyOut();

        yield return new WaitForSeconds(_settings.hideClientTime);

        StopAllCoroutines();
        gameObject.SetActive(false);
        OnClientLeft?.Invoke();

    }

    public void GotBeer()
    {
        if (_currentState == ClientState.Fighting) return;
        if (hasBeer) return;

        hasBeer = true;

        CancelInvoke();
        StopAllCoroutines();

        AnimatorIsWalking(false);
        AnimatorHasBeer(true);

        _currentState = RandomProbability.GetRandom(_settings.returnProbability) ? ClientState.Returned : ClientState.Simple;
        _currentDirection = Direction.Up;
        
        UpdateClientState(); // CAN RETURN OR NOT

        if (_currentState == ClientState.Simple) 
        { 
            StartCoroutine(Movning()); // FLY AWAY
        }
        else
        {
            StartCoroutine(MoveUpThenDrink()); // DRINK AND GOING DOWN
        }
    }

    private void UpdateClientState()
    {
        if (_currentState != ClientState.Returned) return;

        if (_canDrinkBeerMugs <= 0)
            _currentState = ClientState.Simple; // FLY AWAY
        else
            _canDrinkBeerMugs--;
    }

    private void AnimatorHasBeer(bool isActive) => _animator.SetBool(_settings.hasBeer, isActive);
    private void AnimatorIsWalking(bool isActive) => _animator.SetBool(_settings.walk, isActive);
    private void AnimatorFlyOut() => _animator.SetTrigger(_settings.flyOut);

    private void OnDisable()
    {
        if (_settings == null) return;

        _currentDirection = Direction.Down;
        AnimatorHasBeer(false);
        StopAllCoroutines();

        //SLOW MO
        FreezeButton.OnChangeSpeed -= SetSpeed;
    }

    private IEnumerator MoveUpThenDrink()
    {
        if (_lastTime == 0)
            _lastTime = Time.time - _settings.returnTime;
        else
            _lastTime = Time.time;

        while (!CheckLastTime(_settings.returnTime)) // MOVE UP FOR A TIME
        {
            _currentPosition.y += _settings.backSpeed * Time.deltaTime * _gameSpeed;
            transform.position = _currentPosition;

            if (IsAtTopPosition() && _currentDirection == Direction.Up) // TOP POINT
            {
                StartCoroutine(MoveUpAndHide());
                yield break; // EXIT
            }

            yield return null;
        }

        // Drink
        _animator.SetTrigger(_settings.drink);
        yield return new WaitForSeconds(1f);

        
    }
    private void ThrowMugFromAnimation() // CALL FROM ANIMATION
    {
        AnimatorHasBeer(false);
        _currentDirection = Direction.Down;

        if (_currentState == ClientState.Freezed)
            return;

        OnThrowMug?.Invoke(_currnetTable, _currentPosition.y);

        hasBeer = false;

        if (RandomProbability.GetRandom(_settings.tipPlaceProbability))
        {
            OnTipsPlaced?.Invoke(_currnetTable, _currentPosition.y);
        }

        StartCoroutine(WaitAndGoDown());
    }

    private IEnumerator WaitAndGoDown()
    {
        yield return new WaitForSeconds(_settings.stopAfterThrowMug * 1 / _gameSpeed); // DEALAY

        StartCoroutine(Movning()); // GO DOWN
    }

    private void KickBarmen()
    {
        if (!gameObject.activeSelf) return;

        //Debug.Log("KICK BARMEN");
        _currentState = ClientState.Fighting;
        OnClientKickBarmen?.Invoke(_currnetTable);
        StartCoroutine(GoToFight());
    }

    private IEnumerator GoToFight() {

        _lastTime = Time.time;

        while (!CheckLastTime(1f))
        {
            MovePlayer(-1f);
            yield return null;
        }
        gameObject.SetActive(false);
    }

    public void Freeze()
    {
        //if (_currentState == ClientState.Fighting) return;
        AnimatorIsWalking(false);
        StopAllCoroutines();
        CancelInvoke();
        _currentState = ClientState.Freezed;
    }

    public void UpdateOrder(int order) => _sortingGroup.sortingOrder = order;

    private void SetSpeed(float speed = 1f)
    {
        _gameSpeed = speed;
        _animator.speed = speed;
    }
}