﻿using UnityEngine;

[CreateAssetMenu(fileName ="Client", menuName ="ScriptableObjects/Client")]
public class ClientScriptableObjeect : ScriptableObject
{
    [Header("Client Prefab")]
    public Client client = null;

    [Header("Values")]
    public float forwardSpeed = .5f;
    public float backSpeed = 3f;
    public float moveTime = 1f;
    public float stopTime = 3.5f;
    public float kickBarmenDelay = 1f;
    public float hideClientTime = 0.55f;

    [Header("Return")]
    public int maxBeerMugs = 2;
    public float returnTime = 0.5f;
    public float stopAfterThrowMug = 1f;

    [Header("Probabilities")]
    public int returnProbability = 3;
    public int tipPlaceProbability = 3;

    [Header("AnimatorSettings")]
    public string startAnimation = "Idle";
    public string hasBeer = "HasBeer";
    public string drink = "Drink";
    public string walk = "Walk";
    public string flyOut = "FlyOut";
    public string comming = "Comming";

    [Header("Sounds")]
    public AudioClip bleachingSound = null;
    public AudioClip[] heySound = null;

}
