﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public static class PlayerData
{
    public static string LoadStringDataByKey(string key)
    {
        return PlayerPrefs.GetString(key);
    }

    public static void SaveStringDataByKey(string key, string data)
    {
        PlayerPrefs.SetString(key, data);
    }

#if UNITY_EDITOR
    [MenuItem("Tools/ClearScores")]
    public static void CleatPrefs()
    {
        PlayerPrefs.DeleteAll();
    }
#endif
}
