﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class NextScene : MonoBehaviour
{
    void Start()
    {
        SceneManager.LoadScene(1);
    }
}
