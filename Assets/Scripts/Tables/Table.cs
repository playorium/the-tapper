﻿using UnityEngine;

public class Table : MonoBehaviour
{
    [System.Serializable]
    public struct TableClientPoints {
        public Transform clientStartPosition;
        public Transform clientHidePosition;
        public Transform clientEndPosition;
    }
    
    [System.Serializable]
    public struct TableBeerMugPoints
    {
        public Transform statMugPoint;
        public Transform endMugPoint;
        public Transform endOfTablePoint;
    }

    [Header("Clients Points")]
    [SerializeField] private TableClientPoints _clientPoints = new TableClientPoints();
    
    [Header("Barmen Point")]
    [SerializeField] private Transform _barmenPosition = null;

    [Header("Mug Points")]
    [SerializeField] private TableBeerMugPoints _tableMugPoints = new TableBeerMugPoints();

    public TableClientPoints GetClientPoints() => _clientPoints;
    public TableBeerMugPoints GetTableBeerMugPoints() => _tableMugPoints;
    public Transform GetBarmenPosition() => _barmenPosition;
}
