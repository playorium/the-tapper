﻿using UnityEngine;

public class TableController
{
    private static Table[] _tables = null;

    public TableController (Table[] tables)
    {
        _tables = tables;
    }

    public static Table.TableBeerMugPoints GetBeerPoints(int id)
    {
       // if (id < 0 || id >= _tables.Length) return null;
        return _tables[id].GetTableBeerMugPoints();
    }

    public static Table.TableClientPoints GetClientPositions(int id)
    {
        //if (id < 0 || id >= _tables.Length) return;
        return _tables[id].GetClientPoints();
    }

    public static Transform GetBarmenPosition(int id)
    {
        if (id < 0 || id >= _tables.Length) return null;
        return _tables[id].GetBarmenPosition();
    }

    public static int GetTablesCount() => _tables.Length;
}
