﻿using System.Collections;
using System;
using UnityEngine;

[RequireComponent(typeof(Sound), typeof(Collider2D), typeof(Animator))]
public class BeerMug : MonoBehaviour
{
    private Collider2D _collider = null;
    private Sound _sound = null;
    private Animator _animator;

    private BeerMugScriptableObject _settings = null;

    private enum MugState { Full, Empty, Hidden }

    [SerializeField] private GameObject _fullState = null;
    [SerializeField] private GameObject _emptyState = null;

    private MugState _mugState = MugState.Full;
    private Vector3 _startEdgeOfTable ,_endEdgeOfTable;

    private bool _isAlreadyTriggired= false; //PREVENT MULTIPLE TRIGGER
    private bool _isFalling = false;
    private float _gameSpeed = 1f; // MULTIPLIER FOR SLOW MOUTION EFFECT
  
    public static event Action OnMugStartFalling, OnMugFall, OnBarmenGotMug;

    public BeerMug Clone(BeerMugScriptableObject settings)
    {
        GameObject go = Instantiate(gameObject);
        BeerMug beerMug = go.GetComponent<BeerMug>();
        beerMug.Init(settings);
        go.SetActive(false);

        return beerMug;
    }

    public void SetPositions(Vector3 startPosition, Vector3 endPosition, Vector3 endOfTable)
    {
        transform.position = startPosition;
        _endEdgeOfTable = endPosition;
        _startEdgeOfTable = endOfTable;
    }

    public void MakeEmpty()
    {
        SetMugState(MugState.Empty);
        _isAlreadyTriggired = false;

        if (_sound == null) _sound = GetComponent<Sound>();

        _sound?.PlayClip(6); // PLACE EMPTY MUG
        _sound?.PlayClip(7); // THROW IT
    }

    public void Freeze()
    {
        if (!_isFalling)
            StopAllCoroutines();
    }

    public void ResetBeerMug()
    {
        StopAllCoroutines();
        SetMugState(MugState.Full);

        _isFalling = false;
        _isAlreadyTriggired = false;
    }

    private void Init(BeerMugScriptableObject settings)
    {
        if (_sound == null)
            _sound = GetComponent<Sound>();
        if (_collider == null)
            _collider = GetComponent<Collider2D>();
        if (_animator == null)
            _animator = GetComponent<Animator>();

        _settings = settings;
        _mugState = MugState.Full;
    }

    private void OnEnable()
    {
        if (_settings == null) return;
        if (_startEdgeOfTable == null) return;
        
        _collider.enabled = true;
        _animator.Rebind();

        _isAlreadyTriggired = false;

        if (_mugState != MugState.Empty) { 
            _sound.PlayClip(0);
            _sound.PlayClip(7);
        }
        StartCoroutine(Move());

        // SLOW MO
        _gameSpeed = FreezeButton.currentSpeed;
        FreezeButton.OnChangeSpeed += SetSpeed;
    }


    private void OnDisable()
    {
        _collider.enabled = false;
        SetMugState(MugState.Full);
        StopAllCoroutines();

        FreezeButton.OnChangeSpeed -= SetSpeed;
    }

    private IEnumerator Move()
    {
        Vector3 newPosition = Vector3.zero;

        while (true)
        {

            float speed = (_mugState == MugState.Full) ? _settings._speedUp: -1 * _settings._speedDown; // EMPTY
            newPosition = speed * Vector3.up * Time.deltaTime * _gameSpeed;

            transform.position += newPosition;

            if(transform.position.y >= _endEdgeOfTable.y && _mugState == MugState.Full)
            {
                StartCoroutine(FlyOut());
                yield break;
            }
            else if(transform.position.y <= _startEdgeOfTable.y && _mugState == MugState.Empty)
            {
                StartCoroutine(FallDown());
                yield break;
            }

            yield return null;
        }
    }

    private IEnumerator FallDown()
    {
        _collider.enabled = false;
        _isFalling = true;

        OnMugStartFalling?.Invoke();

        _sound.PlayClip(9);

        float lastTime = Time.time;
        Vector3 startPos = transform.position;

        while (Time.time < (lastTime + _settings._fallTime))
        {
            startPos.y -= _settings._fallSpeed * Time.deltaTime;
            transform.position = startPos;
            yield return null;
        }

        _sound.PlayClip(2);
        SetMugState(MugState.Hidden);

        _animator.SetTrigger(_settings.brokeAnim);
        yield return new WaitForSeconds(_settings.brokeAnimTime);

        gameObject.SetActive(false);
        SetMugState(MugState.Full);

        OnMugFall?.Invoke();
    }

    private IEnumerator FlyOut()
    {
        _collider.enabled = false;
        _isFalling = true;
        
        OnMugStartFalling?.Invoke();
        
        _sound.PlayClip(2);
        SetMugState(MugState.Hidden);
        _animator.SetTrigger(_settings.brokeAnim);

        yield return new WaitForSeconds(_settings.brokeAnimTime);

        gameObject.SetActive(false);
        SetMugState(MugState.Full);

        OnMugFall?.Invoke();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (_isAlreadyTriggired || !gameObject.activeSelf) return; // NO MORE TRIGGERS EVENTS

        if (collision.tag.Contains("Barmen")) // BARMEN GET
        {
            if (_mugState == MugState.Full) return;
            
            _sound.PlayClip(1);
            
            OnBarmenGotMug?.Invoke();
            
            gameObject.SetActive(false);
        }
        else // CLIENT GET
        {
            if (_mugState == MugState.Empty) return;
            
            Client client = collision.gameObject.GetComponent<Client>();
            if (client.hasBeer) return;

            _isAlreadyTriggired = true;

            _sound.PlayClip(3);
            gameObject.SetActive(false);
            
            client?.GotBeer();
        }  
    }

    private void SetMugState(MugState mugState)
    {
        _mugState = mugState;

        if(mugState == MugState.Hidden)
        {
            _emptyState.SetActive(false);
            _fullState.SetActive(false);
            return;
        }

        bool isEmpty = _mugState == MugState.Empty;

        _emptyState.SetActive(isEmpty);
        _fullState.SetActive(!isEmpty);
    }

    private void SetSpeed(float speed)
    {
        _gameSpeed = speed;
    }
 }