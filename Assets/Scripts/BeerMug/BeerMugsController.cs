﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BeerMugsController
{
    private BeerMugScriptableObject _beerMug;
    private List<BeerMug> _beerMugs = new List<BeerMug>();
    
    public BeerMugsController(BeerMugScriptableObject beerMug, int mugsCount = 15)
    {
        _beerMug = beerMug;

        // SUBSCRIBE FULL MUG THROW
        Barmen.OnThrowMug -= ThrowFullyMag;
        Barmen.OnThrowMug += ThrowFullyMag;

        // SUBSCRIBE EMPTY MUG THROW
        Client.OnThrowMug -= ThrowEmptyMag;
        Client.OnThrowMug += ThrowEmptyMag;

        if(_beerMugs.Count < 1) // CREATE MUGS
        for (int i = 0; i < mugsCount; i++)
            _beerMugs.Add(CreateBeerMug());
    }

    private BeerMug CreateBeerMug() => _beerMug.beerMug.Clone(_beerMug);

    public void ThrowEmptyMag(int tableId, float yPostion) => ThrowMug( tableId, yPostion );

    public void ThrowFullyMag(int tableId) => ThrowMug( tableId, null );

    public void ThrowMug(int tableID ,float? yPosition)
    {
        Table.TableBeerMugPoints mugPoints = TableController.GetBeerPoints(tableID);
        BeerMug beerMug = GetMug(mugPoints.statMugPoint, mugPoints.endMugPoint, mugPoints.endOfTablePoint);

        if( yPosition != null)
        {
            Vector3 position = beerMug.transform.position;
            position.y = (float) yPosition;
            beerMug.transform.position = position;
            beerMug.MakeEmpty();
        }

        beerMug?.gameObject.SetActive(true);
    }


    public BeerMug GetMug(Transform startPosition, Transform endPosition, Transform endOfTable)
    {
        BeerMug beerMug = null;
        // SEARCH IN PULL
        for (int i = 0; i < _beerMugs.Count; i++)
        {
            if (_beerMugs[i].gameObject.activeSelf) continue;
            
            beerMug = _beerMugs[i];
            break;
        }
        
        // CREATE NEW ONE
        if(beerMug == null)
            beerMug = CreateBeerMug();

        beerMug.SetPositions(startPosition.position, endPosition.position, endOfTable.position);
        return beerMug;
    }

    public int GetActiveBeerMugs()
    {
        int active = 0;

        for(int i = 0; i< _beerMugs.Count; i++)
        {
            if (_beerMugs[i].gameObject.activeSelf) active++;
        }

        return active;
    }

    public void ResetMugs()
    {
        for (int i = 0; i < _beerMugs.Count; i++) {
            _beerMugs[i].ResetBeerMug();
            _beerMugs[i].gameObject.SetActive(false);
        }
    }
    public void FreezeMugs()
    {
        for (int i = 0; i < _beerMugs.Count; i++)
        {
            if(_beerMugs[i].gameObject.activeSelf)
                _beerMugs[i].Freeze();
        }
    }
}
