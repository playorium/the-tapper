﻿using UnityEngine;

[CreateAssetMenu(fileName = "BeerMug", menuName = "ScriptableObjects/BeerMug")]
public class BeerMugScriptableObject : ScriptableObject
{
    [Header("Barmen Prefab")]
    public BeerMug beerMug = null;

    [Header("BeerMug Values")]
    public float _speedUp = 4f;
    public float _speedDown = 1f;
    public float _fallSpeed = 5f;
    public float _fallTime = 0.1f;

    [Header("Animator")]
    public float brokeAnimTime = 1f;
    public string brokeAnim = "Broken";
}
