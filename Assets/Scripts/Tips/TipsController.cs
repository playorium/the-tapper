﻿using System.Collections.Generic;
using UnityEngine;

public class TipsController
{
    private Tip _tip = null;
    private List<Tip> _tips = new List<Tip>();

    public TipsController(Tip tip, int tipsCount = 5)
    {
        _tip = tip;
        _tip.gameObject.SetActive(false);

        Client.OnTipsPlaced -= TipPlace;
        Client.OnTipsPlaced += TipPlace;
        
        for(int i = 0; i < tipsCount; i++)
            _tips.Add(_tip.Clone()); 
    }

    private void TipPlace(int tableID, float yPosition)
    {
        Vector3 position = TableController.GetBeerPoints(tableID).statMugPoint.position;
        position.y = yPosition;
        PlaceTip(position);

    }

    public Tip GetTip()
    {
        for (int i = 0; i < _tips.Count; i++)
            if (!_tips[i].gameObject.activeSelf) return _tips[i];

        Tip tip = _tip.Clone();
        _tips.Add(tip);
        return tip;
    }

    public void PlaceTip(Vector3 position)
    {
        Tip tip = GetTip();
        tip.transform.position = position;
        tip.gameObject.SetActive(true);
    }

    public void ResetTips()
    {
        for (int i = 0; i < _tips.Count; i++)
            _tips[i].gameObject.SetActive(false);
    }
}
