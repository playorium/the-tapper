﻿using System;
using UnityEngine;

[RequireComponent(typeof(Collider2D), typeof(Sound), typeof(Rigidbody2D))]
public class Tip : MonoBehaviour
{
    private Sound _sound = null;
    public static event Action onTipCollected = delegate { };

    void Awake()
    {
        _sound = GetComponent<Sound>();
    }

    public Tip Clone()
    {
        GameObject go = Instantiate(gameObject);
        Tip clone = go.GetComponent<Tip>();
        return clone;
    }

    private void OnEnable()
    {
         _sound.PlayRandom();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        _sound.PlaySound();
        onTipCollected?.Invoke();
        gameObject.SetActive(false);
    }
}
