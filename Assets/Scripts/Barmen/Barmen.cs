﻿using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Barmen: MonoBehaviour
{
    private enum BarmenState { Active, InFight, Happy, Waiting }
    private BarmenState _currentState = BarmenState.Waiting;

    private BarmenScriptableObject _settings = null;

    private Animator _animator;
    private float _fillPercentValue = 0f;

    private float _maxTopPosition;
    private Transform[] _positions = null;
    private int _currentPosition = 0;

    private Sound _sound = null;
    
    public static event Action<int> OnThrowMug = delegate { };
    public static event Action<int> OnStartFilling = delegate { };
    public static event Action<int> OnStopFilling = delegate { };
    public static event Action<Transform> OnStartSwitchPosition = delegate { };

    private void Init(BarmenScriptableObject settings, float maxTopPosition)
    {
        if(_animator == null)
        _animator = GetComponent<Animator>();

        if (_sound == null)
            _sound = GetComponent<Sound>();

        _settings = settings;
        _maxTopPosition = maxTopPosition;

        _sound.SetupSound(_settings.beerFill, new AudioClip[2] {_settings.winSound, _settings.changePosition});
    }

    public Barmen Clone(BarmenScriptableObject settings, float topPosition)
    {
        GameObject go = Instantiate(gameObject);
        Barmen bamren = go.GetComponent<Barmen>();
        bamren.Init(settings, topPosition);

        return bamren;
    }

    public void SetPositions(Transform [] positions) => _positions = positions;

    public void SwitchPositionByID(int newPosition)
    {
        if (_currentState != BarmenState.Active) return;
        if (newPosition < 0 || newPosition >= _positions.Length) return;
        if (_positions[newPosition] == null) return;
        if (newPosition == _currentPosition) return;

        Direction direction = _currentPosition < newPosition ? Direction.Right : Direction.Left;
        RunAnimation(direction);

        OnStartSwitchPosition?.Invoke(_positions[_currentPosition]);

        _currentPosition = newPosition;

        PlaceAtCurrentPosition();
    }

    public void SwitchPositionTo (Direction direction)
    {
        if (_currentState != BarmenState.Active) return;

        if (direction == Direction.Left && _currentPosition <= 0) return;
        else if (direction == Direction.Right && _currentPosition >= _positions.Length-1) return;

        OnStartSwitchPosition?.Invoke(_positions[_currentPosition]);

        StopFillMug();

        _currentPosition += direction == Direction.Right ? 1 : -1;

        PlaceAtCurrentPosition();

        RunAnimation(direction);
    }

    private void PlaceAtCurrentPosition() => transform.position = _positions[_currentPosition].position;
    
    private void RunAnimation(Direction direction)
    {
        if(direction == Direction.Left)
            _animator.SetTrigger(_settings.animatorRunLeft);
        else
            _animator.SetTrigger(_settings.animatorRunRight);
    }

    public void WinAnimation() => _animator.SetTrigger(_settings.animatorWin);

    public void StartFillMugAtBarrelPosition(int position)
    {
        if (_currentState != BarmenState.Active) return;

        if (_currentPosition == position)
            StartFillMug();
    }

    public void StartFillMug()
    {
        if (!gameObject.activeSelf || _currentState != BarmenState.Active) return;

        StopRunUp();

        OnStartFilling?.Invoke(_currentPosition);

        _fillPercentValue = 0f;

        _animator.SetBool(_settings.animatorFill, true);
        _animator.SetFloat(_settings.fillPercent, _fillPercentValue);

        _sound.PlaySound();

        StartCoroutine(FillMug());
    }

    private IEnumerator FillMug()
    {
        while (_fillPercentValue < 1)
        {
            _fillPercentValue += 1 / _settings.fillBeerTime * Time.deltaTime;
            _animator.SetFloat(_settings.fillPercent, _fillPercentValue);
            yield return null;
        }        
    }

    public void ThrowMug()
    {
        OnStopFilling?.Invoke(_currentPosition);
        OnThrowMug?.Invoke(_currentPosition);
        _animator.SetBool(_settings.animatorFill, false);
    }

    public void StopFillMugAtPosition(int position) => StopFillMug();

    public void StopFillMug()
    {
        StopAllCoroutines();

        OnStopFilling?.Invoke(_currentPosition);
        
        _animator.SetBool(_settings.animatorFill, false);
        _animator.SetFloat(_settings.fillPercent, 0f);
        
    }

    public void Activate(int obj)
    {
        _currentState = BarmenState.Active;
    }

    public void RunUp()
    {
        if (!gameObject.activeSelf || _currentState != BarmenState.Active) return;

        StopFillMug();

        _animator.SetBool(_settings.animatorRunUp, true);
        StartCoroutine(RunUpAnimation());
    }

    private IEnumerator RunUpAnimation()
    {
        Vector3 newPosition = transform.position;
        while(transform.position.y < _maxTopPosition)
        {
            newPosition.y += _settings.runUpSpeed * Time.deltaTime;
            transform.position = newPosition;
            yield return null;
        }
    }

    public void StopRunUp()
    {
        if (!gameObject.activeSelf || _currentState == BarmenState.Happy) return;

        StopAllCoroutines();

        if(transform.position.y > _positions[0].position.y)
        _sound.PlayClip(1);

        PlaceAtCurrentPosition(); // RESET TO POINT
        _animator.SetBool(_settings.animatorRunUp, false);
    }

    public void MoveBarmenToFightPos(int pos)
    {
        if (!gameObject.activeSelf || _currentState == BarmenState.InFight) return;

        StopAllCoroutines();
        StopFillMug();
        _animator.SetBool(_settings.animatorRunUp, true);
        _currentPosition = pos;
        _sound.PlayClip(1);
        PlaceAtCurrentPosition();

        _currentState = BarmenState.InFight;
    }

    public void ResetAfterLifeLoss()
    {
        _currentState = BarmenState.Waiting;
        _animator.Rebind();
        PlaceAtCurrentPosition();
        gameObject.SetActive(true);
    }

    public void ResetBarmen()
    {
        _currentState = BarmenState.Waiting;
        StopAllCoroutines();
        _animator.Rebind();
        _currentPosition = 0;
        PlaceAtCurrentPosition();
        gameObject.SetActive(false);
    }

    public void Freeze()
    {
        _currentState = BarmenState.Waiting;
        StopAllCoroutines();
        StopFillMug();
    }

    public void MugFall()
    {
        Freeze();
        _animator.SetTrigger(_settings.animatorMugFall);
    }

    public void GameOver()
    {
        _animator.SetTrigger(_settings.animatorGameOver);
    }

    public void ShowHappy()
    {
        StopFillMug();

        _currentState = BarmenState.Happy;
        _animator.SetTrigger(_settings.animatorWin);
    }
 
    public void StopHappy()
    {
        StopRunUp();
        PlaceAtCurrentPosition();
        _currentState = BarmenState.Waiting;
        _animator.Rebind();
    }
}