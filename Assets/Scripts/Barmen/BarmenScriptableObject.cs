﻿using UnityEngine;

[CreateAssetMenu(fileName = "Barmen", menuName = "ScriptableObjects/Barmen")]
public class BarmenScriptableObject : ScriptableObject
{
    [Header("Barmen Prefab")]
    public Barmen barmen = null;

    [Header("Animator Strings")]
    public string animatorRunRight = "RunRight";
    public string animatorRunLeft = "RunLeft";
    public string animatorRunUp = "RunUp";
    public string animatorFill = "Fill";
    public string animatorWin = "Win";
    public string fillPercent = "FillPercent";
    public string animatorMugFall = "MugFall";
    public string animatorGameOver = "GameOver";

    [Header("Barmen Values")]
    public float fillBeerTime = 0.8f;
    public float runUpSpeed = 3f;

    [Header("Sounds")]
    public AudioClip beerFill = null;
    public AudioClip winSound = null;
    public AudioClip changePosition = null;
}
