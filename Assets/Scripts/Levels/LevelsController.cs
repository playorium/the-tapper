﻿using System;
using System.Collections;
using UnityEngine;

public class LevelsController 
{
    private LevelScriptableObject[] _levels = null;
    private GameController _gameController = null;
    private ClientGenerator _clientsGenerator = null;
    private TableController _tableController = null;

    private int[] _clientsPerTable = null;

    public int currentLevel { get; private set; } = 0;
    private int _maxClientsPerLevel = 0;
    private int _maxClientsPerTable = 0;

    public static event Action onLevelComplete = delegate { };
    public static event Action onGameWin = delegate { };
    public static event Action<int> OnStartLevel = delegate { };

    public LevelsController(LevelScriptableObject[] levels , GameController gameController , Table[] tables)
    {
        _levels = levels;
        _gameController = gameController;
        _tableController = new TableController(tables);

        Client.OnClientLeft -= CheckLevelEnd;
        Client.OnClientLeft += CheckLevelEnd;
    }

    public void StartLevel()
    {
        //Debug.Log("Level:" + currentLevel);
        _gameController.ResetInGameObjects(false);

        SetupClientGenerator();

        if (!_levels[currentLevel].isEndLessLevel)
            CreateFixedClients();
        else 
        {
            CreateFixedClients(true); // BAD CODE NEED FIX
            _gameController.StartCoroutine(CreateClients());
        }

        OnStartLevel?.Invoke(currentLevel);
        _gameController.StartCoroutine(UpdateRendersClients());
    }

    private void SetupClientGenerator()
    {
        SetMaxClientsAtLevel();
        StoreClientsPerTable();

        if (_clientsGenerator == null)
            _clientsGenerator = new ClientGenerator();

        _clientsGenerator.ClearClientsPool();
        _clientsGenerator.SetupClientGenerator(_levels[currentLevel].clients, _maxClientsPerLevel);
    }

    private void StoreClientsPerTable()
    {
        int[] clientPerTable = _levels[currentLevel].tableClientsPerLevel;
        int lenght = clientPerTable.Length;
        _clientsPerTable = new int[lenght];
        Array.Copy(clientPerTable, _clientsPerTable, lenght);
    }

    private void SetMaxClientsAtLevel()
    {
        if (_levels[currentLevel].isEndLessLevel)
        {
            _maxClientsPerLevel = _levels[currentLevel].maxClients;
            return;
        }

        _maxClientsPerTable = 0;

        for (int i = 0; i < _levels[currentLevel].tableClientsPerLevel.Length; i++)
        {
            int clientsAtTable = _levels[currentLevel].tableClientsPerLevel[i];
            if (_maxClientsPerTable < clientsAtTable) _maxClientsPerTable = clientsAtTable;

            _maxClientsPerLevel += clientsAtTable;
        }
    }

    private void CreateFixedClients(bool isEndLessLevel = false)
    {
        float offsetY = 0;

        for(int j = 0; j < _maxClientsPerTable; j++) {

            for (int i = 0; i < _clientsPerTable.Length; i++)
            {
                if (_clientsPerTable[i] <= 0) continue; 

                Client client =_clientsGenerator.GetClientAtPosition(i, offsetY);
                client?.gameObject.SetActive(true);
                _clientsPerTable[i]--;
            }

            offsetY -= _levels[currentLevel].clientOffsetY;
        }

        _maxClientsPerLevel = isEndLessLevel ? 20 : 0; // HARDCODE
    }

    private IEnumerator CreateClients()
    {
        var waitForADelay = new WaitForSeconds(_levels[currentLevel].clientGenerationDelay);

        while (true)
        {
            if (_clientsGenerator.GetActiveClients() >= _maxClientsPerLevel )
                yield return waitForADelay;

            Client client = _clientsGenerator.GetClientAtRandomPosition();
            if (client == null) break;

            client.gameObject.SetActive(true);

            if (DecreaseClientsAndCheck())  // EXIT IF NO CLIENTS
                break;
            
            yield return waitForADelay;
        }
    }

    private bool DecreaseClientsAndCheck()
    {
        if (_levels[currentLevel].isEndLessLevel) return false;
        
        _maxClientsPerLevel--;
        return _maxClientsPerLevel <= 0;
    }

    private IEnumerator UpdateRendersClients()
    {
        var waitForADelay = new WaitForSeconds(_levels[currentLevel].updateClientsRendererOrder);
        while (true)
        {
            _clientsGenerator.UpdateClientRenders();
            yield return waitForADelay;
        }
    }

    private void CheckLevelEnd()
    {
        if (_maxClientsPerLevel <= 0 && _clientsGenerator.GetActiveClients() <= 0) EndLevel();
    }

    private void EndLevel()
    {
        _gameController.StopAllCoroutines();

        currentLevel++;

        if(currentLevel >= _levels.Length)
        {
            onGameWin?.Invoke();
            return;
        }

        onLevelComplete?.Invoke();
        _gameController.Invoke(nameof(StartLevel), UIController.LevelCompleteDelay);
    }

    public void ResetLevel()
    {
        _gameController.CancelInvoke();
        _gameController.StopAllCoroutines();
        _gameController.ResetInGameObjects();
        _clientsGenerator?.ResetClients(); // HIDE CLIENTS
    }

    public void ResetToStart()
    {
        ResetLevel();
        currentLevel = 0;        
    }

    public void FreezeCients()
    {
        _gameController.StopAllCoroutines();
        _clientsGenerator?.FreezeClients();
    }
}