﻿using UnityEngine;

[CreateAssetMenu(fileName ="LevelAsset", menuName ="ScriptableObjects/LevelAsset")]
public class LevelScriptableObject : ScriptableObject
{
    [Space]
    public bool isEndLessLevel = false;

    [Header("Clients At Level")]
    public ClientScriptableObjeect[] clients = null;

    [Header("Clients properties")]
    public int maxClients = 20;
    public float clientGenerationDelay = 2f;
    public float updateClientsRendererOrder = 0.2f;
    public float clientOffsetY = 0.5f;

    [Space]
    public int[] tableClientsPerLevel = new int[4];
}
