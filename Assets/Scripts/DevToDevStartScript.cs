﻿using UnityEngine;

public class DevToDevStartScript : MonoBehaviour
{
    public string androidAppId = "";
    public string androidAppSecret = "";

    void Start()
    {
#if UNITY_ANDROID
        // <param name="androidAppId"> devtodev App ID for Google Play version of application </param>
        // <param name="androidAppSecret"> devtodev Secret key for Google Play version of application </param>
        DevToDev.Analytics.Initialize(androidAppId, androidAppSecret);
#elif UNITY_IOS
// <param name="iosAppId"> devtodev App ID for App Store version of application </param>
// <param name="iosAppSecret"> devtodev Secret key for App Store version of application </param>
   DevToDev.Analytics.Initialize(string iosAppId, string iosAppSecret);
#elif UNITY_WEBGL
// <param name="webglAppId"> devtodev App ID for Web version of application </param>
// <param name="webglAppKey"> devtodev Secret key Web version of application </param>
   DevToDev.Analytics.Initialize(string webglAppId, string webglAppSecret);
#elif UNITY_STANDALONE_WIN
// <param name="winAppId"> devtodev App ID for Windows Store version of application </param>
// <param name="winAppSecret"> devtodev Secret key for Windows Store version of application </param>
   DevToDev.Analytics.Initialize(string winAppId, string winAppSecret);
#endif
    }
};