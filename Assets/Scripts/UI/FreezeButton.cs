﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FreezeButton : MonoBehaviour
{
    [SerializeField] private Image _timer = null;
    [SerializeField] private Image _icon = null;
    [SerializeField] private TextMeshProUGUI _counter = null;

    [SerializeField] private float _timeScale = 0.1f;
    [SerializeField] private float _timerDelay = 5f;
    private float _nextTime = 0f;

    public static event Action<float> OnChangeSpeed = delegate { };

    public static float currentSpeed { get; private set; } = 1f;

    [Header("Colors")]
    [SerializeField] private Color _colorIconNormal = Color.white;
    [SerializeField] private Color _colorIconActive = Color.white;
    [SerializeField] private Color _colorTextNormal = Color.white;
    [SerializeField] private Color _colorTextnActive = Color.white;

    private bool _isActive  = false;

    public void OnClick()
    {
        if (_isActive) return;
        _isActive = true;
        UpdateColors();

        currentSpeed = (_timeScale <= 0f) ? 0.1f : _timeScale;

        OnChangeSpeed?.Invoke(currentSpeed);

        StartCoroutine(TimeUpdate());
    }

    private IEnumerator TimeUpdate()
    {
        _nextTime = Time.time + _timerDelay;
       
        while (Time.time < _nextTime)
        {
            float time = Time.time;

            _counter.text = Mathf.Ceil(_nextTime - time).ToString();
            _timer.fillAmount = 1 - (_nextTime - time) / _timerDelay ;
                                    
            yield return null;
        }

        ResetTime();
    }

    private void OnEnable()
    {
        ResetTime();
    }

    private void UpdateColors()
    {
        _icon.color     = !_isActive ? _colorIconNormal : _colorIconActive;
        _counter.color  = !_isActive ? _colorTextNormal : _colorTextnActive;
    }

    private void ResetTime()
    {
        _timer.fillAmount = 0f;
        _counter.text = "S";

        currentSpeed = 1f;

        OnChangeSpeed?.Invoke(currentSpeed);

        _isActive = false;
        UpdateColors();
    }
}
