﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "CameraAspect" , menuName ="ScriptableObjects/CameraAspec")]
public class CameraAspectsSizes : ScriptableObject, IComparable<CameraAspectsSizes>
{
    public float aspecRaito = 0.42f;
    public float cameraSize = 6.6f;

    public int CompareTo(CameraAspectsSizes other) // INVERSE SORTING
    {
        if (aspecRaito > other.aspecRaito)
            return -1;
        else if (aspecRaito < other.aspecRaito)
            return 1;
        else
            return 0;
    }
}
