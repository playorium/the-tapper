﻿using UnityEngine;

public class ShowTitle: MonoBehaviour
{
    private Animator _animator = null;
    private float _hideDelay = 2f;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    public void SetDelay(float delay)
    {
        _hideDelay = delay;
    }
 
    private void OnEnable()
    {
        _animator.Rebind();
        Invoke(nameof(Hide), _hideDelay);
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }
}
