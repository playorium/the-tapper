﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] private UIScriptableObject _settings = null;

    [Header("Titles")]
    [SerializeField] private GameObject _mainMenu = null;
    [SerializeField] private GameObject _levelTitle = null;
    [SerializeField] private GameObject _prepareServeTitle = null;
    [SerializeField] private GameObject _levelCompleteTitle = null;
    [SerializeField] private GameObject _gameOverTitle = null;

    [Header("LevelCrores")]
    [SerializeField] private TextMeshProUGUI _levelNumber = null;
    [SerializeField] private TextMeshProUGUI _levelSmall = null;

    [Header("Lives")]
    [SerializeField] private GameObject[] _lives = null;
    [SerializeField] private TextMeshProUGUI _livesText = null;

    [Header("Scores")]
    [SerializeField] private GameObject _scores = null;
    [SerializeField] private GameObject _empty = null;
    [SerializeField] private TextMeshProUGUI _text = null;

    [Header("Enter YourName")]
    [SerializeField] private GameObject _enterYourNameGO = null;
    [SerializeField] private TextMeshProUGUI _enterYourNameScores = null;
    [SerializeField] private TextMeshProUGUI _enterYourNameText = null;
    [SerializeField] private TMP_InputField _enterYourNameInput = null;
    [SerializeField] private Button _enterNameOkButton = null;
    [SerializeField] private Color _enerNameActive = Color.white;
    [SerializeField] private Color _enterNameDisabled = Color.white;
    private ScoresEnterName _scoresEnterName = null;

    [Header("HeighScores")]
    [SerializeField] GameObject _highScoreGO = null;
    public HighScores _highScores { get; private set; } = null;
    [SerializeField] private ScoresField _scoreField = null;

    [Header("HeighScores")]
    [SerializeField] private GameObject _freezeButton = null;

    public static float PrepToServeDelay { get; private set; }
    public static float LevelCompleteDelay { get; private set; }
    public static float GameOverDelay { get; private set; }

    public static event Action OnStartLevel = delegate { };
    public static event Action OnGameOverEnd = delegate { };

    private int _currentScores = 0;

    private void Awake()
    {
        ShowMainMenu();

        InitTitle(_levelTitle, _settings.levelTitleDelay);
        InitTitle(_prepareServeTitle, _settings.prepareToServesDelay);
        InitTitle(_levelCompleteTitle, _settings.levelCompleteDelay);
        InitTitle(_gameOverTitle, _settings.gameOverDelay);

        ShowSmallLevel(false);

        PrepToServeDelay = _settings.prepareToServesDelay;
        LevelCompleteDelay = _settings.levelCompleteDelay;
        GameOverDelay = _settings.gameOverDelay;

        SetLives(0);

        _highScores = new HighScores(_scoreField);
        InitEnterName();
    }

    private void InitEnterName()
    {
        _scoresEnterName = new ScoresEnterName(_enterYourNameGO,
                                               _enterYourNameScores,
                                               _enterYourNameText,
                                               _enterYourNameInput,
                                               _enterNameOkButton,
                                               _enerNameActive,
                                               _enterNameDisabled);
    }

    private void InitTitle(GameObject title, float delay)
    {
        title.SetActive(false);
        ShowTitle titleAnim = title.AddComponent<ShowTitle>();
        titleAnim.SetDelay(delay);
    }

    public void SetLives(int lives)
    {
        _livesText.text = SpriteFont.ShowText(lives, SpriteFont.FontType.Mugs);
        /*for (int i = 0; i < _lives.Length; i++)
        {
            if (i < lives)
                _lives[i].SetActive(true);
            else
                _lives[i].SetActive(false);
        }*/
    }

    // MAIN TITLES
    public void ShowMainMenu(bool isActive = true)
    {
        if(isActive) SetScoresInMenu();
        _mainMenu.SetActive(isActive);
    }
    public void ShowPrepareServeTitle(bool isActive = true) => _prepareServeTitle.SetActive(isActive);
    public void ShowLevelCompleteTitle(bool isActive = true) => _levelCompleteTitle.SetActive(isActive);
    public void ShowGameOverTitle(bool isActive = true) => _gameOverTitle.SetActive(isActive);
    
    //ANIMATIONS
    public void ShowStartLevelAnim(Action levelStart, int level = 0) => StartCoroutine(LevelAnim(levelStart, level));
    public void ShowLevelCompleteAnimation(Action levelStart, int level = 1) => StartCoroutine(LevelComplete(levelStart, level));
    public void ShowGameOverAnimation(Action onComplete) => StartCoroutine(GameOver(onComplete));

    // LEVEL TITLES
    private void SetLevelTitleText(int level) => _levelNumber.text = (level + 1).ToString();
    public void ShowLevelTitle(bool isActive = true) => _levelTitle.SetActive(isActive);
    public void SetSmallLevelNumber(int level) => _levelSmall.text = SpriteFont.ShowText(level + 1, SpriteFont.FontType.Level , 2);
    public void ShowSmallLevel(bool isActive = true) => _levelSmall.gameObject.SetActive(isActive);

    // SCORES

    public void ShowHighScores(bool isActive = true) => _highScoreGO.gameObject.SetActive(isActive);
    public void ShowEnterName(bool isActive) => _enterYourNameGO.SetActive(isActive);

    // FREEZEBUTTONS

    public void ShowFreezeButton(bool isActive) => _freezeButton.SetActive(isActive);
    
    private IEnumerator LevelAnim(Action levelStart, int level = 0)
    {
        ShowFreezeButton(false);

        SetLevelTitleText(level);
        ShowLevelTitle();
        yield return new WaitForSeconds(_settings.levelTitleDelay);

        OnStartLevel?.Invoke();

        SetSmallLevelNumber(level);
        ShowPrepareServeTitle();
        yield return new WaitForSeconds(_settings.prepareToServesDelay);
        
        levelStart?.Invoke();

        ShowFreezeButton(true);
    }

    private IEnumerator LevelComplete( Action levelStart, int level = 0)
    {
        ShowFreezeButton(false);
        ShowLevelCompleteTitle();
        yield return new WaitForSeconds(_settings.levelCompleteDelay);
        StartCoroutine(LevelAnim(levelStart, level));
    }

    private IEnumerator GameOver(Action onComplete)
    {

        ShowFreezeButton(false);

        ShowGameOverTitle();
        yield return new WaitForSeconds(_settings.gameOverDelay);

        OnGameOverEnd?.Invoke(); // NEED FOR STOP BARMEN

        if (onComplete != null)
            onComplete?.Invoke();
        else
            ResetAndShowMenu();
    }

    public void StopAnimations()
    {
        StopAllCoroutines();
        CancelInvoke();
    }

    public void ResetAndShowMenu()
    {
        StopAnimations();

        ShowMainMenu();
        ShowFreezeButton(false);
        ShowLevelTitle(false);
        ShowPrepareServeTitle(false);
        ShowGameOverTitle(false);
        ShowSmallLevel(false);
        ShowEnterName(false);
        ShowHighScores(false);
       
        SetLevelTitleText(0);
        SetSmallLevelNumber(0);
    }

    public void SetScoresInMenu()
    {
        int scores = ScoresData.GetHighScore();
        if (scores <= 0) { 
            ShowHideBestScores(false);
            return;
        }
        
        _text.text = ScoresData.PrepareScoreText(scores);
        ShowHideBestScores();
    }

    public void ShowHideBestScores(bool isActive = true)
    {
        _scores.SetActive(isActive);
        _empty.SetActive(!isActive);
    }

    private  void TryToShowEnterYourName()
    {
        if (!ScoresData.IsHighScore(_currentScores))
        {
            ResetAndShowMenu();
            _currentScores = 0;
            return;
        }
        _scoresEnterName.ShowWindow(_currentScores, _highScores.SaveScores, ResetAndShowMenu);
        _currentScores = 0;
    }

    public void SaveScoresAndReturnMainMenu(int scores, bool isNeedGameOver = false)
    {
        _currentScores = scores;

        if (isNeedGameOver)
            ShowGameOverAnimation(TryToShowEnterYourName);
        else
            TryToShowEnterYourName();
    }
}