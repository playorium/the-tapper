﻿using UnityEngine;

public class SpriteFont
{
    public enum FontType {Scores = 0, Mugs = 10, Scores2 = 20, Mugs2 = 30, Level = 40}

    public static string ShowText(int value, FontType currentFont, int width = 0)
    {
        string textBuf = Mathf.Abs(value).ToString();
        string output = "";

        if (width > 0) textBuf = AddPrefix(textBuf, width);

        foreach (char c in textBuf)
        {
            output = string.Concat(output, GetSymlbolScores(int.Parse(c.ToString()), currentFont));
        }
        return output;
    }

    private static string AddPrefix (string value, int width)
    {
        for (int i = value.Length; i < width; i++)
        {
            value = string.Concat("0",value);
        }
        return value;
    }

    private static string GetSymlbolScores(int number, FontType currentFont)
    {
        if ( number < 0 || number > 9) return "";

        //number = number == 0 ? number = 9 : number-1;
        return GetNumber(number + (int)currentFont);
    }

    private static string GetNumber(int number) => $"<sprite={number}>";
}
