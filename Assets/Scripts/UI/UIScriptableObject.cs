﻿using UnityEngine;

[CreateAssetMenu(fileName = "UISettings", menuName ="ScriptableObjects/UISettings")]
public class UIScriptableObject : ScriptableObject
{
    [Header("Titles Delay")]
    public float levelTitleDelay = 1f;
    public float prepareToServesDelay = 1.5f;
    public float levelCompleteDelay = 3f;
    public float gameOverDelay = 2.3f;
}
