﻿using UnityEngine;
using System;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;

[ExecuteInEditMode]
#endif
public class CameraDimensions : MonoBehaviour
{
    private Camera _camera = null;
    
    [Header("Aspect Settings")]
    [SerializeField] private CameraAspectsSizes[] _settings = null;
    [SerializeField] private float _comparePercision = 0.01f;

#if UNITY_EDITOR
    public bool ShowAspectRaito = false;
#endif
    private void Awake() => _camera = GetComponent<Camera>();

    private void OnEnable()
    {
        Array.Sort(_settings);
        _settings = _settings.Distinct().ToArray();

        UpdateDimensions();
    }

    private void UpdateDimensions()
    {
        if (_settings == null) return;
        if (_settings.Length <= 0) return;

        if (_camera.aspect >= _settings[0].aspecRaito) { 
            SetCameraSize(0);
            return;
        }
        else if(_camera.aspect < _settings[_settings.Length - 1].aspecRaito) 
        {
            SetCameraSize(_settings.Length - 1);
            return;
        }

        for (int i = 0; i< _settings.Length ; i++)
        {
            if (Mathf.Abs(_settings[i].aspecRaito - _camera.aspect) < _comparePercision) // EQUAL SIZE
            {
                SetCameraSize(i);
                return;
            }
            
            if(_camera.aspect >= (_settings[i+1].aspecRaito + _comparePercision) && // APROXIMATE SIZE
                _camera.aspect < _settings[i].aspecRaito)
            {
                SetAproximateSize(i, i + 1);
                return;
            }
        }
    }

    private void SetCameraSize(int currentAspect)
    {
        if (_settings[currentAspect] == null) return;
        _camera.orthographicSize = _settings[currentAspect].cameraSize;
    }

    private void SetAproximateSize(int startAspectID, int endAspectID)
    { 
        float percent = Mathf.InverseLerp(_settings[endAspectID].aspecRaito, _settings[startAspectID].aspecRaito, _camera.aspect);
        float newSize = Mathf.Lerp(_settings[startAspectID].cameraSize, _settings[endAspectID].cameraSize, 1-percent);
       _camera.orthographicSize = newSize;
    }

    private void OnApplicationPause(bool pause)
    {
        if (!pause)
        UpdateDimensions();
    }

#if UNITY_EDITOR
    private void Update()
    {
        if(ShowAspectRaito)
        Debug.Log(_camera.aspect);
        UpdateDimensions();    
    }
#endif
}
