﻿using TMPro;
using UnityEngine;

public class ScoresField: MonoBehaviour
{
    [SerializeField] private CanvasGroup _canvasGorup;
    [SerializeField] private TextMeshProUGUI _nameText, _scoresText;

    private void OnValidate()
    {
        _canvasGorup = GetComponent<CanvasGroup>();
        TextMeshProUGUI[] _fields = gameObject.GetComponentsInChildren<TextMeshProUGUI>();
        _nameText = _fields[0];
        _scoresText = _fields[1];

        FillEmpty();
    }

    private void FillEmpty()
    {
        _scoresText.text = "";
        _nameText.text = "Empty";
        _canvasGorup.alpha = 0.3f;
    }

    public ScoresField Clone()
    {
        Transform parent = transform.parent;
        
        GameObject go =  Instantiate(gameObject);
        go.transform.SetParent(parent);
        go.transform.localScale = Vector3.one;
        
        ScoresField scoreField = go.GetComponent<ScoresField>();
        
        return scoreField;
    }

    public void UpdateScores ( string name = "Empty" , int scores = 0 )
    {
        if(scores <= 0)
        {
            FillEmpty();
            return;
        }

        _canvasGorup.alpha = 1f;
        _scoresText.text = ScoresData.PrepareScoreText(scores);
        _nameText.text = name;
    }
}