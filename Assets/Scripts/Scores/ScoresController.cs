﻿using TMPro;

public class ScoresController
{
    private ScoresScriptableObjects _scoreSettings = null;
    private int _currentScores = 0;
    private int _scoreStep;
    private TextMeshProUGUI _textField;

    public ScoresController(ScoresScriptableObjects scoreSettings, TextMeshProUGUI textField)
    {
        _scoreSettings = scoreSettings;
        _scoreStep = 5;
        _textField = textField;

        Client.OnClientLeft -= ScoresAddClientLeft;
        Client.OnClientLeft += ScoresAddClientLeft;

        BeerMug.OnBarmenGotMug -= ScoresBarmenGotMug;
        BeerMug.OnBarmenGotMug += ScoresBarmenGotMug;

        Tip.onTipCollected -= ScoresTipCollected;
        Tip.onTipCollected += ScoresTipCollected;

        LevelsController.onLevelComplete -= ScoresLevelComplete;
        LevelsController.onLevelComplete += ScoresLevelComplete;

        LevelsController.onGameWin -= ScoresGameWin;
        LevelsController.onGameWin += ScoresGameWin;
    }

    private void ScoresGameWin()
    {
        ScoresAdd(_scoreSettings.bonusLevel);
    }

    private void ScoresLevelComplete()
    {
        ScoresAdd(_scoreSettings.levelComplete);
    }

    private void ScoresTipCollected()
    {
        ScoresAdd(_scoreSettings.collectTip);
    }

    public void RestScore()
    {
        _currentScores = 0;
        UpdateScores();
    }

    public void ScoresMugFlyOut()
    {
        _currentScores -= _scoreStep;
        UpdateScores();
    }

    public void ScoresBarmenGotMug()
    {
        ScoresAdd(_scoreSettings.collectMug);
    }

    public void ScoresAddClientLeft()
    {
        ScoresAdd(_scoreSettings.simpleClient);
    }

    public void ScoresAdd(int score)
    {
        _currentScores += score;
        UpdateScores();
    }

    private void UpdateScores()
    {
        _textField.text = SpriteFont.ShowText(_currentScores, SpriteFont.FontType.Scores);
    }

    public int GetScores()
    {
        return _currentScores;
    }
}
