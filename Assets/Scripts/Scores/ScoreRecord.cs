﻿using System;
using UnityEngine;

[Serializable]
public class ScoreRecord: IComparable<ScoreRecord>
{
    [SerializeField] private string _name = "Empty";
    [SerializeField] private int _scores = 0;
    public string name { get { return _name; } }
    public int scores { get { return _scores; } }

    public ScoreRecord( int newScores = 0, string newName = "")
    {
        _name = newName;
        _scores = newScores;

        SetRecord(_scores, _name);
    }

    public void SetRecord(int newScores, string newName = "")
    {
        if (newScores < 0) return;
        _scores = newScores;
        _name = newName.Equals("") ? "No name" : newName;
    }

    public int CompareTo(ScoreRecord other)
    {
        if (_scores > other._scores)
            return -1;
        else if (_scores < other._scores)
            return 1;
        else
            return 0;
    }
}