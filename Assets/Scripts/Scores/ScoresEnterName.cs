﻿using UnityEngine.UI;
using UnityEngine;
using TMPro;
using System;

public class ScoresEnterName
{
    private GameObject _window          = null;
    private TextMeshProUGUI _scoresText = null;
    private TextMeshProUGUI _nameText   = null;
    private TMP_InputField _input       = null;
    private Button _okButton            = null;
    private Action<int, string> _onEnter = delegate { };
    private Action _onWindowClose;
    private Color _activeColor,_emptyColor;

    private string _placeHolder = "";
    private int _scores = 0;

    public ScoresEnterName ( GameObject window,
                            TextMeshProUGUI scores,
                            TextMeshProUGUI text,
                            TMP_InputField input, 
                            Button okButton, 
                            Color activeColor, 
                            Color emptyColor, 
                            string placeHolderText="Your name")
    {
        _window = window;
        _scoresText = scores;
        _nameText = text;
        _input = input;
        _okButton = okButton;
        _activeColor = activeColor;
        _emptyColor = emptyColor;
        _placeHolder = placeHolderText;

        Initilizate();
    }

    private void Initilizate()
    {
        ResetNameText();

        _okButton.onClick.RemoveAllListeners();
        _okButton.onClick.AddListener(EnterName);

        _input.onValueChanged.RemoveAllListeners();
        _input.onValueChanged.AddListener(CheckInput);
    }

    private void ResetNameText()
    {
        _nameText.text = _placeHolder;
        _nameText.color = _emptyColor;
    }

    private void UpdateInput()
    {
        _input.text = _placeHolder;
    }

    public void ShowWindow(int scores, Action<int, string> onEnter, Action onWindowsClose)
    {
        _scores = scores;
        _scoresText.text = ScoresData.PrepareScoreText(_scores);

        ResetNameText();
     
        _onEnter = onEnter;
        _onWindowClose = onWindowsClose;

        _window.SetActive(true);

        UpdateInput();
        ActivateField(false);
    }

    private void CheckInput(string value)
    {
        value = value.Trim();

        if (string.IsNullOrEmpty(value))
            ActivateField(false);
        else
            ActivateField(true);
    }

    private void ActivateField(bool isActive = true)
    {
        _nameText.color = isActive ? _activeColor : _emptyColor;
        _nameText.text  = isActive ? _nameText.text   : _placeHolder;
    }

    private void EnterName()
    {
        _onEnter?.Invoke(_scores, _nameText.text);
        _onWindowClose?.Invoke();
        _window.SetActive(false);
    }
}
