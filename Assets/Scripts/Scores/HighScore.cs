﻿using UnityEngine;

public class HighScores
{
    private const int _maxRecords = 10;

    private ScoreRecord[] _scoreRecords = new ScoreRecord[_maxRecords];
    private ScoresField[] _scoreFields = new ScoresField[_maxRecords];

    public int minScore { get; private set; } = 0;
    public int maxScore { get; private set; } = 0;

    public HighScores ( ScoresField scoreField )
    {
        LoadingScores();
        CreateFields(scoreField);
        UpadteRecordsView();
    }

    private void LoadingScores()
    {
        _scoreRecords = ScoresData.LoadHightScore();
        
        if (_scoreRecords == null)  CreateEmptyRecords();
    }

    private void CreateFields(ScoresField scoreField)
    {
        _scoreFields = new ScoresField[_maxRecords];
        Transform parent = scoreField.transform.parent;

        _scoreFields[0] = scoreField;
        for ( int i = 1 ; i < _maxRecords ; i++ )
            _scoreFields[i] = scoreField.Clone();
    }

    private void UpadteRecordsView()
    {
        System.Array.Sort(_scoreRecords);

        for ( int i = 0 ; i < _maxRecords ; i++ )
        _scoreFields[i].UpdateScores(_scoreRecords[i].name, _scoreRecords[i].scores);

        ScoresData.SaveHightScore(_scoreRecords);
    }

    public void CreateRandomRecords()
    {
        Debug.Log("Random scores");
        
        _scoreRecords = new ScoreRecord[_maxRecords];

        for ( int i = 0 ; i < _maxRecords ; i++ )
        {
            int scores = Random.Range(0, 90000);
            int randomName = Random.Range(0, 5);
            string name = "";
            switch (randomName)
            {
                case 0: name = "Vasya"; break;
                case 1: name = "Misha"; break;
                case 2: name = "Alyosha"; break;
                case 3: name = "Genadiy"; break;
                case 4: name = "Sergey"; break;
                case 5: name = "Pasha"; break;
            }

            _scoreRecords[i] = new ScoreRecord(scores, name);
            Debug.Log($"{name} {scores}");
        }

        UpadteRecordsView();
    }

    public void SaveScores(int scores, string name)
    {
        int i = 0;
        while(_scoreRecords[i].scores >= scores && i<_scoreRecords.Length-1)
            i++;

        ScoreRecord newRecord = new ScoreRecord(scores, name);

        if (i < _scoreRecords.Length - 1)
        {
            ScoreRecord[] tempRecords = new ScoreRecord[_scoreRecords.Length - 1 - i];
            System.Array.Copy(_scoreRecords, i, tempRecords, 0, tempRecords.Length);
            _scoreRecords[i] = newRecord;
            System.Array.Copy(tempRecords, 0, _scoreRecords, i + 1, tempRecords.Length);
        }
        else
            _scoreRecords[i] = newRecord;

        UpadteRecordsView();
    }

    public void AddHighScores(int scores, string name)
    {
        int i = 0;
        while(_scoreRecords[i].scores > scores)
        {
            i++;
        }
        Debug.Log(i);
    }

    public void CreateEmptyRecords()
    {
        _scoreRecords = new ScoreRecord[_maxRecords];

        for (int i = 0; i < _maxRecords; i++)
            _scoreRecords[i] = new ScoreRecord(0, "Empty");
    }
}