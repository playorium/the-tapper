﻿using UnityEngine;

public static class ScoresData
{
    private const string HIGHT_SCORES = "highScores";
    private static ScoreRecord[] _scores = null;

    [System.Serializable]
    private class ScoresDataClass {
        [SerializeField]
        public ScoreRecord[] records = null;
    }

    public static void SaveHightScore(ScoreRecord[] records)
    {
        ScoresDataClass data = new ScoresDataClass();
        data.records = records;
        string json = JsonUtility.ToJson(data);
        PlayerData.SaveStringDataByKey(HIGHT_SCORES, json);

        _scores = records;
    }

    public static ScoreRecord[] LoadHightScore()
    {
        string json = PlayerData.LoadStringDataByKey(HIGHT_SCORES);
        ScoresDataClass data = JsonUtility.FromJson<ScoresDataClass>(json);

        if (data == null) return null;

        _scores = data.records;

        return data.records;
    }

    public static string PrepareScoreText(int scores) => string.Format("{0:n0}", scores);

    public static bool IsHighScore(int score) => (_scores != null) ?  score > _scores[_scores.Length - 1].scores : true;
    public static int GetHighScore()
    {
        if(_scores == null)
            _scores = LoadHightScore();

        return (_scores != null) ? _scores[0].scores : 0;
    }
}