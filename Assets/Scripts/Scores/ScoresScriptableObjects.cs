﻿using UnityEngine;

[CreateAssetMenu(fileName ="Scores", menuName ="ScriptableObjects/Scores")]
public class ScoresScriptableObjects : ScriptableObject
{
    [Header("Scores table")]
    public int simpleClient = 50;
    public int sportClient = 75;
    public int crazyClient = 100;
    public int alienClinet = 150;
    public int collectMug = 100;
    public int collectTip = 500;
    public int levelComplete = 1000;
    public int bonusLevel = 3000;
}
