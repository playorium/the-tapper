﻿using UnityEngine;

public class Sound : MonoBehaviour
{
    [SerializeField] private AudioClip _clip = null;
    [SerializeField] private AudioClip[] _clips = null;

    [Tooltip("Rare coeficent\nRandom(0, RareFactor) < RareFactor  - 1 ")]
    [SerializeField] private int _rareFactor = 3;

    public void SetupSound(AudioClip clip, AudioClip[] clips)
    {
        _clip = clip;
        _clips = clips;
        //_rareFactor = rareFactor;
    }
    
    public void PlaySound()
    {
        if (_clip == null) return;
        SoundPlayer.instance.Play(_clip);
    }

    public void PlayClip(int clipID)
    {
        if (clipID < 0 || clipID >= _clips.Length) return;
        if (_clips[clipID] == null) return;

        SoundPlayer.instance.Play(_clips[clipID]);
    }

    public void PlayRandom()
    {
        AudioClip clip = _clips[Random.Range(0, _clips.Length)];
        if (clip == null) return;

        SoundPlayer.instance.Play(clip);
    }

    public void PlayAudioClip(AudioClip clip)
    {
        if (clip == null) return;
        SoundPlayer.instance.Play(clip);
    }

    public void PlayRandomAudioClip(AudioClip[] clips)
    {
        if (clips == null) return;

        int clipID = Random.Range(0, clips.Length);
        if (clips[clipID] == null) return;

        SoundPlayer.instance.Play(clips[clipID]);
    }

    public void PlaySoundWithLuck()
    {
        if (!RandomProbability.GetRandom(_rareFactor)) return;

        SoundPlayer.instance.Play(_clip);
    }

    public void PlayRandomWithLuck()
    {
        if (!RandomProbability.GetRandom(_rareFactor)) return; 

        AudioClip clip = _clips[Random.Range(0, _clips.Length)];
        if (clip == null) return;

        SoundPlayer.instance.Play(clip);
    }
}
