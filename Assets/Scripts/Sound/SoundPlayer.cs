﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundPlayer : MonoBehaviour
{
    public static SoundPlayer instance;
    private AudioSource _audioSource = null;

    private void Awake()
    {
        instance = this;
        _audioSource = GetComponent<AudioSource>();
    }
    public void Play(AudioClip clip, float volume = 1f)
    {
        _audioSource.PlayOneShot(clip, volume);
    }
}
