﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MusicPlayer : MonoBehaviour
{
    private AudioSource _audioSource = null;

    [SerializeField] private AudioClip[] _musics = null;

    private int _currentSong = 0;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        _audioSource.playOnAwake = false;
        _audioSource.loop = true;
        _audioSource.Stop();
    }

    private void Start()
    {
        PlaySong(0);
    }

    public void MenuSong()
    {
        PlaySong(0);
    }

    public void GameSong()
    {
        PlaySong(1);
    }

    public void NextSong()
    {
        _currentSong++;
        if (_currentSong >= _musics.Length)
            _currentSong = 0;

        PlaySong(_currentSong);
    }

    private void PlaySong(int id)
    {
        _audioSource.clip = _musics[id];
        _audioSource.Play();
    }
}
