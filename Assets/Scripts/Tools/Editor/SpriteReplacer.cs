﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class SpriteReplacer : EditorWindow
{
    private struct SpriteVariation
    {
        public string name;
        public Sprite sprite;
    }

    private List<Sprite> _sprites = new List<Sprite>();
    private List<SpriteRenderer> _spriteRenderers = new List<SpriteRenderer>();
    List<SpriteVariation> _spritesVariations = new List<SpriteVariation>();

    private Vector2 _scroll = Vector2.zero;
    private string _spriteName = "";

    [MenuItem("Tools/SpriteReplacer")]
    public static void Init()
    {
        SpriteReplacer window = (SpriteReplacer)EditorWindow.GetWindow(typeof(SpriteReplacer));
        window.Show();
    }

    private void OnGUI()
    {
        if (Selection.transforms.Length <= 0)
        {
            GUILayout.Label("PLEASE SELECT OBJECTS IN SCENE HIERARCHY", EditorStyles.boldLabel);
            return;
        }
        
        if (GUILayout.Button("GET SPRITES FROM SELECTED OBJECTS IN SCENE"))
        {
            GetSpritesFromSelectedObjects();
        }

        if (_sprites.Count == 0) return;
        
        GUILayout.Space(10f);
        GUILayout.Label("PLEASE ENTER PREFIX OR POSTFIX FOR A SPRITES. THE SEARCH WAS BY OTHER PART", EditorStyles.boldLabel);
        GUILayout.Space(10f);

        _scroll = GUILayout.BeginScrollView(_scroll);

        ShowSprites();

        GUILayout.EndScrollView();
    }

    private void GetSpritesFromSelectedObjects()
    {
 
        Transform[] transforms = Selection.transforms;
        _sprites = new List<Sprite>();
        _spriteRenderers = new List<SpriteRenderer>();

        if (transforms.Length == 0)
        {
            Debug.LogWarning("PLEASE SELECT GAMEOBJECTS IN SCENE HIERARCHY");
            return;
        }

        foreach (Transform t in transforms)
        {
            SpriteRenderer[] spriteRenderers = t.gameObject.GetComponentsInChildren<SpriteRenderer>(true);
            foreach (SpriteRenderer spriteRender in spriteRenderers)
            {
                _sprites.Add(spriteRender.sprite);
            }
            _spriteRenderers.AddRange(spriteRenderers);
        }

        Debug.Log($"{_sprites.Count} SPRITES WAS FOUNDED");
    }

    private void ShowSprites()
    {
        SearchSameSprites();

        GUILayout.Space(10f);
        if (_spritesVariations.Count > 0)
        {
            ShowSwitchSpritesButtons();
            GUILayout.Space(10f);
        } 

        for (int i = 0; i < _sprites.Count; i++)
        {
            GUILayout.BeginHorizontal();
            _sprites[i] = EditorGUILayout.ObjectField(_sprites[i], typeof(Sprite)) as Sprite;
            GUILayout.EndHorizontal();
        }
    }

    private void SearchSameSprites()
    {
        GUILayout.BeginHorizontal();

        if (string.IsNullOrEmpty(_spriteName)) _spriteName = _sprites[0].name;
        _spriteName = GUILayout.TextField(_spriteName);

        if (GUILayout.Button("SEARCH SPRITES VARIATIONS"))
        {
            _spritesVariations = new List<SpriteVariation>();
            SearchAssetsWithSprtitesInProject(_spriteName);
        }

        GUILayout.EndHorizontal();
    }

    private void SearchAssetsWithSprtitesInProject(string spriteNameMask)
    {
        foreach (var sprite in _sprites) { 

            string spriteName = sprite.name.Replace(spriteNameMask, "");
            string[] guids = AssetDatabase.FindAssets($"{spriteName} t:Sprite", new[] { "Assets" });
            guids = guids.Distinct().ToArray();

            _spritesVariations.AddRange (SearchSpriteVariationsInAssets(guids, spriteName));
        }
    }

    private List<SpriteVariation> SearchSpriteVariationsInAssets(string[] guids, string spriteName) // SEARCH ALL ATLASES WITH SPRITES
    {
        List<SpriteVariation> spriteVariations = new List<SpriteVariation>();

        foreach (var guid in guids)
        {
            spriteVariations.AddRange(LoadSpritesVariationsFromAtlas(guid, spriteName));
        }
        return spriteVariations;
    }

    private List<SpriteVariation> LoadSpritesVariationsFromAtlas(string guid, string name) // GET ALL SPRITES FROM ATLAS AND STORE IT
    {
        Object[] objects = AssetDatabase.LoadAllAssetRepresentationsAtPath(AssetDatabase.GUIDToAssetPath(guid));
        if (objects.Length <= 0) return null;

        List<SpriteVariation> sprites = new List<SpriteVariation>(); 

        foreach(var obj in objects)
        {
            if (!obj.name.Contains(name)) continue;

            SpriteVariation sv = new SpriteVariation();
            sv.sprite = (Sprite)obj;
            sv.name = obj.name.Replace(name, "");
            
            sprites.Add(sv);
        }
        return sprites;
    }

    private int GetSpritesCount(string name) => _spritesVariations.Where(s => s.name == name).Count();
    
    private void ShowSwitchSpritesButtons() // SHOW PART WITH BUTTONS AND LABEL TAP TO SWITCH TO SELECTED
    {
        var unique =  _spritesVariations.Select(s=>s.name).Distinct();

        GUILayout.BeginHorizontal();

        GUILayout.Label("TAP TO BUTTON TO SWITCH SPRITES", EditorStyles.boldLabel);

        GUILayout.EndHorizontal();
        GUILayout.Space(10f);
        GUILayout.BeginHorizontal();

        foreach (var prefix in unique)
        {
            if(GUILayout.Button($"{prefix} ({GetSpritesCount(prefix)})"))
            {
                SwitchSprites(prefix);
            }
        }
        GUILayout.EndHorizontal();
    }

    private void SwitchSprites(string prefix) // SWITCH ALL SPRITES IN SPRITERENDERERS TO SELECTED
    {
        var currentSprites = _spritesVariations.Where(s => s.name == prefix).ToArray();
        int replacedSpritesCount = currentSprites.Length;

        for (int i = 0; i < _spriteRenderers.Count; i++) { 
           
            string sprite = $"{_spriteRenderers[i].sprite.name} = ";
            string changedSprite =  (i < replacedSpritesCount) ? currentSprites[i].sprite.name : "None";
            
            //TEMP
           // var newSprite = currentSprites.Where(s => s.name == _spriteRenderers[i].sprite.name).Distinct();
           /// changedSprite = newSprite.ToList()[0].name;
            
            if( i < replacedSpritesCount )  _spriteRenderers[i].sprite = currentSprites[i].sprite; // REPLACE SPRITES

            Debug.Log(string.Concat(sprite, changedSprite));
        }
    }
}
