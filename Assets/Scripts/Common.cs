﻿
using UnityEngine;

public enum Direction { Left, Right, Up, Down, None };

public static class RandomProbability
{
    /// <summary>
    /// Return true or false if percent <= 0 return false. If percent = 1 true\n
    /// if percent = 2 50/50, percent = 3 20/10, percent = 4 30/10 and so on...
    /// </summary>
    /// <param name="percent"></param>
    /// <returns></returns>
    public static bool GetRandom(int percent)
    {
        if (percent <= 0) return false;
        else if (percent == 1) return true;
        return Random.Range(0, percent) >= percent-1;
    }
}