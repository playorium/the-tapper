﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuffController 
{
    private struct Puff
    {
        public GameObject go;
        public Animator animator;
    }
    private Puff _puff;
    private List<Puff> _puffsList = new List<Puff>();

    private GameController _gameController = null;
    private GameObject _puffPrefab = null;
    private Animator _animator = null;
    private int _countOfPuffs = 0;
    private float _timeToPlay = 0;

    public PuffController(GameController gameController, GameObject puffPrefab, int count = 4, float timeToPlay = 1.5f)
    {
        _gameController = gameController;

        _puffPrefab = puffPrefab;
        _animator = _puffPrefab.GetComponent<Animator>();

        _countOfPuffs = count;
        _timeToPlay = timeToPlay;

        GeneratePuffsPool();

        SubsrbePuffsToBarmenEvents();
    }

    public void ResetPuffs()
    {
        for (int i = 0; i < _puffsList.Count; i++)
        {
            _puffsList[i].go.SetActive(false);
        }
    }

    private void GeneratePuffsPool()
    {
        _puff = new Puff();
        _puff.go = _puffPrefab;
        _puff.go.SetActive(false);
        _puff.animator = _animator;

        for (int i = 0; i < _countOfPuffs; i++)
        {
            _puffsList.Add(DuplicatePuff());
        }
    }

    private Puff DuplicatePuff()
    {
        Puff puffNew = new Puff();
        puffNew.go = GameObject.Instantiate(_puff.go);
        puffNew.animator = puffNew.go.GetComponent<Animator>();
        return puffNew;
    }

    private void SubsrbePuffsToBarmenEvents()
    {
        Barmen.OnStartSwitchPosition -= ShowPuff;
        Barmen.OnStartSwitchPosition += ShowPuff;
    }

    private void ShowPuff(Transform transform)
    {
        Puff puff = GetPuff();
        if (puff.go == null) return;

        puff.go.transform.position = transform.position;
        puff.go.SetActive(true);
        _gameController.StartCoroutine(ShowPuffForTime(puff, _timeToPlay));
    }

    private Puff GetPuff()
    {
        for(int i = 0; i<_puffsList.Count; i++)
        {
            if (!_puffsList[i].go.activeSelf)
            {
                _puffsList[i].animator.Rebind();
                return _puffsList[i];
            }
        }
        _puffsList.Add(DuplicatePuff());
        return _puffsList[_puffsList.Count-1];
    }

    private IEnumerator ShowPuffForTime(Puff puff, float time)
    {       
        yield return new WaitForSeconds(time);
        puff.go.SetActive(false);
    }
}
