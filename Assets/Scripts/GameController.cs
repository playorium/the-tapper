﻿using System.Collections;
using TMPro;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [Header("Levels")]
    [SerializeField] private LevelScriptableObject[] _levels = null;
    private LevelsController _levelsController = null;

    [Header("Barmen")]
    [SerializeField] private  BarmenScriptableObject _barmenSettings = null;
    [SerializeField] private Transform _barmenTopPosition = null;
    [SerializeField] private GameObject _fightPrefab = null;
    private Barmen _barmen;

    [Header("Puffs")]
    [SerializeField] private GameObject _puffPrefab = null;
    private PuffController _puffContoller = null;

    [Header("Table")]
    [SerializeField] private Table[] _tables = null;
    
    [Header("BeerMug")]
    [SerializeField] private BeerMugScriptableObject _beerMug = null;
    [SerializeField] private BeerMugsController _beerMugsController = null;

    [Header("Tips")]
    [SerializeField] private Tip _tip = null;
    private TipsController _tipsController = null;

    [Header("Scores")]
    [SerializeField] private ScoresScriptableObjects _scoreSettings = null;
    [SerializeField] private TextMeshProUGUI _scoresText = null;
    private ScoresController _scoreController = null;
    
    [Header("Input")]
    [SerializeField] private InputController _inputController = null;
    private BarrelController _barrelController = null;

    [Header("MainMenu & UI")]
    private UIController _uiController = null;
    [SerializeField] int _maxLives = 3;
    private int _lives = 3;

    private bool isFreezing = false;

    private void Awake()
    {
        _fightPrefab = Instantiate(_fightPrefab);
        _fightPrefab.SetActive(false);
        _barrelController = GetComponent<BarrelController>();
        _uiController = GetComponent<UIController>();

        // SETUP SCORES
        if (_scoreController == null)
            _scoreController = new ScoresController(_scoreSettings, _scoresText);
    }

    public void StartGame(bool inputType = false)
    {
        _lives = _maxLives;
        _uiController.SetLives(_lives);
        _uiController.ShowMainMenu(false);
        _uiController.ShowSmallLevel();
        _uiController.ShowLevelTitle();

        _inputController.gameObject.SetActive(!inputType);
        _barrelController.isActive = inputType;
        _barrelController.ResetBarrels();

        // PREPARE MUGS POOL
        if (_beerMugsController == null)
            _beerMugsController = new BeerMugsController(_beerMug);

        // PREPARE TIPS POOL
        if (_tipsController == null)
            _tipsController = new TipsController(_tip, 5);

        // PREAPRE BARMEN
        SetupBarmen();

        // PREPARE BARMEN PUFFS
        if (_puffContoller == null)
            _puffContoller = new PuffController(this, _puffPrefab, 4, 1.5f);

        SubscribeToGameEvents();

        // RUN GAME
        if(_levelsController == null)
            _levelsController = new LevelsController(_levels, this, _tables);

        _uiController.ShowStartLevelAnim(_levelsController.StartLevel, _levelsController.currentLevel);

    }
        
    private void SubscribeToGameEvents()
    {
        LevelsController.onLevelComplete -= LevelComplete;
        LevelsController.onLevelComplete += LevelComplete;

        LevelsController.onGameWin -= GameWin;
        LevelsController.onGameWin += GameWin;

        Client.OnClientKickBarmen -= KickBarmen;
        Client.OnClientKickBarmen += KickBarmen;

        BeerMug.OnMugStartFalling -= OnMugStartFalling;
        BeerMug.OnMugStartFalling += OnMugStartFalling;

        BeerMug.OnMugFall -= LiveLoss;
        BeerMug.OnMugFall += LiveLoss;

        UIController.OnStartLevel -= StopBarmenHappy;
        UIController.OnStartLevel += StopBarmenHappy;
        UIController.OnGameOverEnd -= StopBarmenSad;
        UIController.OnGameOverEnd += StopBarmenSad;

        LevelsController.OnStartLevel -= _barmen.Activate;
        LevelsController.OnStartLevel += _barmen.Activate;

        _barrelController.SubcribeBarrels();
    }

    private void StopBarmenHappy()
    {
        _barmen.StopHappy();
    }

    private void StopBarmenSad()
    {
        _barmen?.ResetBarmen(); // DISABLE BARMEN
    }

    private void SetupBarmen() // NEED TO REFACTOR IT
    {
        if (_barmen == null)
            _barmen = _barmenSettings.barmen.Clone(_barmenSettings, _barmenTopPosition.position.y);

        // START POINTS OF BARMEN
        Transform[] barmenPositions = new Transform[_tables.Length];
        for (int i = 0; i < _tables.Length; i++)
        {
            barmenPositions[i] = _tables[i].GetBarmenPosition();
        }

        _barmen.SetPositions(barmenPositions);

        InputController inputController = 
            (_inputController.gameObject.activeInHierarchy) ? _inputController : _barrelController.GetInput();

        InputEventsConnector coonectBarmenToInput = 
            new InputEventsConnector(_barmen, inputController, _barrelController);

        _barmen.ResetAfterLifeLoss();
    }

    private void KickBarmen(int pos)
    {
        if (isFreezing) return;
        FreezeObjects();
        StartCoroutine(KickBarmenAnim(pos));
    }

    private IEnumerator KickBarmenAnim(int pos)
    {
        _barmen.StopRunUp();
        _barmen?.Freeze();
        yield return new WaitForSeconds(0.4f);
        _barmen.MoveBarmenToFightPos(pos);

        yield return new WaitForSeconds(0.5f);
        _fightPrefab.transform.position = _tables[pos].GetBarmenPosition().position;
        _fightPrefab.SetActive(true);
        yield return new WaitForSeconds(2.5f);
        _fightPrefab.SetActive(false);

        LiveLoss();
    }

    private void OnMugStartFalling()
    {
        if (isFreezing) return;
        FreezeObjects();
        _barmen?.MugFall();
    }

    private void LiveLoss() {
        if (_fightPrefab.gameObject.activeSelf) return;

        _lives--;
        _uiController.SetLives(_lives);

        if (_lives <= 0) { 
            GameLose();
            return;
        }

        ResetInGameObjects();
        _levelsController.ResetLevel();

        _uiController.ShowStartLevelAnim(_levelsController.StartLevel, _levelsController.currentLevel);
    }

    private void LevelComplete()
    {
        //Debug.Log("LevelComplete");
        ResetInGameObjects(false);
        _barmen?.ShowHappy();
        _uiController.ShowLevelCompleteAnimation(_levelsController.StartLevel, _levelsController.currentLevel);
    }

    public void StopGame()
    {
        _uiController.StopAnimations();
        _uiController.SaveScoresAndReturnMainMenu(_scoreController.GetScores());
        ResetGame();
        FreezeObjects();
    }

    private void GameLose()
    {
        _uiController.SaveScoresAndReturnMainMenu(_scoreController.GetScores(), true);
        
        ResetGame(true);

        _barmen?.ResetAfterLifeLoss();
        _barmen?.GameOver();
    }

    private void GameWin()
    {
        Debug.Log("Your Win!");
        ResetGame();
        _uiController.ResetAndShowMenu();
    }

    private void ResetGame(bool isGameOver = false)
    {     
        StopAllCoroutines(); // STOP MAIN LOOP
        _levelsController?.ResetToStart(); // RESET LEVELS

        ResetInGameObjects();

        if(!isGameOver)_barmen?.ResetBarmen(); // DISABLE BARMEN

        _scoreController?.RestScore(); // RESET SCORES
    }

    private void FreezeObjects()
    {
        if (isFreezing) return;
        isFreezing = true;

        _beerMugsController?.FreezeMugs(); // STOP ALL MUGS
        _levelsController?.FreezeCients(); // STOP ALL CLIENTS
    }

    public void ResetInGameObjects(bool isNeedResetBarmen = true)
    {
        isFreezing = false;

        _fightPrefab.SetActive(false);
        _beerMugsController?.ResetMugs(); // HIDE MUGS
        _tipsController?.ResetTips(); // HIDE TIPS
        _puffContoller.ResetPuffs();

        if(isNeedResetBarmen)
        _barmen?.ResetAfterLifeLoss();
    }
}
