﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BarrelController : MonoBehaviour
{
    [SerializeField] private InputController _inputController = null;

    public delegate void PositionClick(int position);
    public event PositionClick OnPositionClick, OnTouchPosition, OnTouchEndPosition;

    private HashSet<Barrel> _barrels = new HashSet<Barrel>();

    public bool isActive = true;

    public InputController GetInput() => _inputController;

    public void ClickAt(int position)
    {
        if (!isActive) return;
        OnPositionClick?.Invoke(position);
    }

    public void TouchAt(int position)
    {
        if (!isActive) return;
        OnTouchPosition?.Invoke(position);
    }

    public void TouchEndAt(int position)
    {
        if (!isActive) return;
        OnTouchEndPosition?.Invoke(position);
    }

    public void AddBarrel(Barrel barrel)
    {
        _barrels.Add(barrel);
    }

    public void ClearBarrels()
    {
        _barrels.Clear();
    }

    public void ResetBarrels()
    {
        foreach (Barrel barrel in _barrels)
        {            
            barrel.ResetBarrel();
        }
    }

    public void SubcribeBarrels()
    {
        foreach (Barrel barrel in _barrels)
        {
            Barmen.OnStartFilling -= barrel.HideCran;
            Barmen.OnStartFilling += barrel.HideCran;
            Barmen.OnStopFilling -= barrel.ShowCran;
            Barmen.OnStopFilling  += barrel.ShowCran;
        }
    }
}
