﻿public class InputEventsConnector
{
    public InputEventsConnector(Barmen barmen, InputController input, BarrelController barrels){

        // INPUT EVENTS SWIPE
        input.onSwipeEvent -= barmen.SwitchPositionTo;
        input.onSwipeEvent += barmen.SwitchPositionTo;

        input.OnTapEvent -= barmen.StartFillMug;
        input.OnTapEvent += barmen.StartFillMug;

        input.OnTapEndEvent -= barmen.StopFillMug;
        input.OnTapEndEvent += barmen.StopFillMug;

        input.OnTopTapEvent -= barmen.RunUp;
        input.OnTopTapEvent += barmen.RunUp;

        input.OnTopTapEndEvent -= barmen.StopRunUp;
        input.OnTopTapEndEvent += barmen.StopRunUp;

        // BARRELS TAP EVENTS 
        barrels.OnPositionClick -= barmen.SwitchPositionByID;
        barrels.OnPositionClick += barmen.SwitchPositionByID;

        barrels.OnTouchPosition -= barmen.StartFillMugAtBarrelPosition;
        barrels.OnTouchPosition += barmen.StartFillMugAtBarrelPosition;

        barrels.OnTouchEndPosition -= barmen.StopFillMugAtPosition;
        barrels.OnTouchEndPosition += barmen.StopFillMugAtPosition;
    }
}
