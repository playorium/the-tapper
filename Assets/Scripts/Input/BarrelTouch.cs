﻿using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Collider2D))]
public class BarrelTouch : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private int _position = 0;
    public int position { get => _position; }

    [SerializeField] private BarrelController _barrelController = null;
    public BarrelController barrelController { get => _barrelController; }

    public void OnPointerClick(PointerEventData eventData)
    {
        _barrelController.ClickAt(_position);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _barrelController.TouchAt(_position);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _barrelController.TouchEndAt(_position);
    }

#if UNITY_EDITOR
    private void Init()
    {
        _position = transform.GetSiblingIndex();
        gameObject.name = $"Barrel {_position.ToString()}";
        _barrelController = FindObjectOfType<BarrelController>();
    }
    public void OnValidate()
    {
        if (string.IsNullOrEmpty(gameObject.scene.name)) return;
        Init();
    }
    public void Reset()
    {
        if (string.IsNullOrEmpty(gameObject.scene.name)) return;
        Init();
    }
#endif
}
