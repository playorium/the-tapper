﻿using UnityEngine;

[RequireComponent(typeof(BarrelTouch))]
public class Barrel : MonoBehaviour
{
    [SerializeField]
    private GameObject _cran = null;
    private int _currentPoisition = 0;
    private BarrelTouch _barrelTouch = null;
    private BarrelController _barrelController = null;

    private void Awake()
    {
        _barrelTouch = GetComponent<BarrelTouch>();
        _currentPoisition = _barrelTouch.position;
    }

    private void Start()
    {
        _barrelController = _barrelTouch.barrelController;
        _barrelController.AddBarrel(this);
    }

    public void ResetBarrel()
    {
        _cran.SetActive(true);
    }

    public void ShowCran(int pos)
    {
        if (pos != _currentPoisition) return;
        _cran.SetActive(true);
    }

    public void HideCran(int pos)
    {
        if (pos != _currentPoisition) return;
        _cran.SetActive(false);
    }
}
