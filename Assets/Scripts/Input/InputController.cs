﻿using System.Collections;
using UnityEngine;

public class InputController : MonoBehaviour
{
    [SerializeField] private float _delayTapEvent = 0.1f;

    private float _lastPosition = 0f; // NEED TO GET DIRECTION

    // SWIPE
    public delegate void OnSwipe(Direction direction);
    public event OnSwipe onSwipeEvent = null;

    // TAP
    public delegate void OnTap();
    public event OnTap OnTapEvent, OnTapEndEvent, OnTopTapEvent, OnTopTapEndEvent;

    private bool _isSwipe, _isTapping = false;

    private Direction _tapArea = Direction.None;

    public void OnPoinerDown(bool isTop) // FIRST EVENT
    {
        _tapArea = isTop ? Direction.Up : Direction.None;
        StartCoroutine(TapStart());
    }

    private IEnumerator TapStart() // DALAYED START FOR TAP AND SWIPE SEPARATION
    {
        _isSwipe = false;

        yield return new WaitForSeconds(_delayTapEvent); // DELAY FOR USER CAN START SWIPE
        if (_isSwipe) yield break;

        _isTapping = true;

        OnTap tapEvent = IsTopTap() ? OnTopTapEvent : OnTapEvent; // TOP TAP OR REGULAR ONE
        tapEvent?.Invoke();
    }

    private bool IsTopTap() => _tapArea == Direction.Up;

    public void OnBeginDrag() // IF SWIPE
    {
        if (_isTapping) return;
        StopAllCoroutines(); // STOP TAP

        _lastPosition = Input.mousePosition.x;
        _isSwipe = true; // START SWIPE
    }

    public void OnEndDrag() // END SWIPE
    {
        float offset = (_lastPosition - Input.mousePosition.x);
        onSwipeEvent?.Invoke(GetSwipeDirrection(offset)); 
    }

    private Direction GetSwipeDirrection(float value) =>            // SWIPE DIRECTION
        Mathf.Sign(value) > 0 ? Direction.Left : Direction.Right;

    public void OnPointerUP() // LAST EVENT
    {
        if (_isSwipe)
        {
            _isSwipe = false;
            return;
        }

        StopAllCoroutines();

        OnTap tapEvent = IsTopTap() ? OnTopTapEndEvent : OnTapEndEvent;
        tapEvent?.Invoke();

        _isTapping = false;
    }
}